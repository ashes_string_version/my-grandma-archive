---
title:  2021-04-20
aliases: []
tags: [dailynote]
---

# Symptoms
- กลืนไม่ลง คายอาหาร
- พูดไม่ได้?
- เจ็บคอ

# Story
- ประมาณ 3-4 วันก่อน [[Su]]: "[[Grandma]]ไม่ใช่เจ็บคอธรรมดา"
- [[Su]] ใช้ syringe ให้อาหาร