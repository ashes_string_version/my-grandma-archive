---
title:  Grandma
aliases: []
tags: [person, family]
---
# Grandma

- รักษาที่ [[VJ Clinic]] โดย [[ATAPOL SUGHONDHABIROM|Atapol]] และให้โอนเงินไปบัญชีชื่อ [[จรูญศักดิ์ เฮ้งตระกูล]] ไม่แน่ใจว่าเป็นผู้ช่วยหรือไม่
- ใช้ครีมนวดของ [[P]]
- [[MOM]], [[DAD]], [[Su]] fam
- โรคประจำตัว [[hypertension]]

# symtom by Observing

- [[Swelling of feet and ankles]]
- [[inceased need to pee (at night)]]
- [[loss of appetite]]
- [[weakness]]
- [[weight loss]]
- [[Haematuria]]
- [[abdominal pain]] - lower right [[low back pain on one side]]
    - with 7cm+ (handful) size hard tumor in lower right [[a mass one the side or lower back]]
        - can be obseved by eyes and touch
- [[Fatigue]]
