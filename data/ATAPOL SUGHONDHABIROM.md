---
title:  ATAPOL SUGHONDHABIROM
aliases: [อรรถพล สุคนธาภิรมย์ ณ พัทลุง, Atapol]
tags: [person]
---
# ATAPOL SUGHONDHABIROM
- atapol.s@gmail.com
- 21 Publications 
- 1300 Reads 
- 357 Citations

# Organisation
- [[VJ Clinic]]

# Research

- [[Influence of DAP1 Genotype and Psychosocial Factors on Posttraumatic Stress Disorder in Thai Tsunami Survivors: A GxE Approach]]
- [[Adjunctive minocycline treatment for major depressive disorder: A proof of concept trial]]
- [[Genome-Wide Association Study in Thai Tsunami Survivors Identified Risk Alleles for Posttraumatic Stress Disorder]]
- https://www.researchgate.net/profile/Atapol-Sughondhabirom


# Links
- https://www.md.chula.ac.th/%E0%B8%99%E0%B8%B2%E0%B8%A2%E0%B9%81%E0%B8%9E%E0%B8%97%E0%B8%A2%E0%B9%8C%E0%B8%AD%E0%B8%A3%E0%B8%A3%E0%B8%96%E0%B8%9E%E0%B8%A5-%E0%B8%AA%E0%B8%B8%E0%B8%84%E0%B8%99%E0%B8%98%E0%B8%B2%E0%B8%A0%E0%B8%B4/
- https://www.md.chula.ac.th/en/mr-atapol-sughondhabirom/
- https://opencorporates.com/companies/th/0105556129567

# Diagnose
