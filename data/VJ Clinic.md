---
title:  VJ Clinic
aliases: [VJ CLINIC COMPANY LIMITED]
tags: [organisation]
---
# VJ Clinic

Incorporation date: 2013-08-15 #process

[[Retail sale of pharmaceutical and medical goods in specialized stores]]
[[Retail sale of pharmaceutical and medical goods, cosmetic and toilet articles in specialized stores]]
[[จำหน่ายเวชภัณฑ์]]

# Officers
- [[วิลิต เตชะไพบูลย์]]
- [[พิชาญ จิรฏฐิติกาล]]
- [[จรูญศักดิ์ เฮ้งตระกูล]] SCB 3782081859
- [[ATAPOL SUGHONDHABIROM]]

# Links
- https://opencorporates.com/companies/th/0105556129567