---
title:  leptospirosis
aliases: [โรคฉี่หนู]
tags: [disease]
---

# leptospirosis

- https://www.cdc.gov/leptospirosis/index.html


# Symptoms

- [[fever]]
- [[chills]]
- [[vomiting]]
- [[abdominal pain]]
- [[rash]]
- [[Diarrhea]]
- [[headache]]