---
title:  chronic granulomatous disease
aliases: [chronic granulomatous infection, CGD]
tags: [disease]
---

# chronic granulomatous disease
- https://www.mayoclinic.org/diseases-conditions/chronic-granulomatous-disease/symptoms-causes/syc-20355817
- 

# Symptoms
- [[pneumonia]]
- [[fever]]
- [[chest pain]]
- [[runny nose]]
- [[skin irritation]] [[rash]] [[swelling]] [[redness]]
- [[stomach pain]]
- [[abdominal pain]]
- [[vomiting]]
- [[Diarrhea]]