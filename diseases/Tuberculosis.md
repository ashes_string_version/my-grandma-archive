---
title:  Tuberculosis
aliases: [TB, วัณโรค]
tags: [disease]
---

# Tuberculosis
- https://www.mayoclinic.org/diseases-conditions/tuberculosis/symptoms-causes/syc-20351250
- 

# Symptoms
- [[chest pain]]
- [[coughing]]
- [[weight loss]]
- [[Fatigue]]
- [[fever]]
- [[chills]]
- [[loss of appetite]]