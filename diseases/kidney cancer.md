---
title:  kidney cancer
aliases: []
tags: [disease]
---

# kidney cancer

- [[kidney cancer]] can cause [[chronic kidney disease|CKD]]

# Symptoms
- [[Haematuria]]
- [[low back pain on one side]]
    - pain in your back or side that doesn't go away
- [[a mass one the side or lower back]]
- [[Fatigue]]
- [[loss of appetite]]
- [[weight loss]]
- [[fever]]
- [[anemia]]