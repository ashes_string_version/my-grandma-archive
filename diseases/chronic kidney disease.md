---
title:  chronic kidney disease
aliases: [ไตวาย, CKD, chronic kidney failure]
tags: [disease]
---

# chronic kidney disease

- https://www.mayoclinic.org/diseases-conditions/chronic-kidney-disease/symptoms-causes/syc-20354521
- Chronic kidney disease, also called chronic kidney failure, describes the gradual loss of kidney function. Your kidneys filter wastes and excess fluids from your blood, which are then excreted in your urine. When chronic kidney disease reaches an advanced stage, dangerous levels of fluid, electrolytes and wastes can build up in your body.
- In the early stages of chronic kidney disease, you may have few signs or symptoms. Chronic kidney disease may not become apparent until your kidney function is significantly impaired.
- [[eGFR test]]
- urine test - protien in the urine

# Symptoms

- [[Nausea]]
- [[vomiting]]
- [[loss of appetite]]
- [[Fatigue]] [[weakness]]
- [[Sleep problems]]
- [[muscle cramps]]
- [[Swelling of feet and ankles]]
- [[hypertension]]
- [[shortness of breath]]
- [[Haematuria]] - blood in you pee
- [[inceased need to pee (at night)]]
- [[weight loss]]