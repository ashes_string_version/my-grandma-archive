---
title:  pseudotuberculosis
aliases: [tb เทียม]
tags: [disease]
---

# pseudotuberculosis

- https://www.health.govt.nz/your-health/conditions-and-treatments/diseases-and-illnesses/yersiniosis
- [[Tuberculosis|TB]] เทียม

# Symptoms
- [[vomiting]]
- [[fever]]
- [[abdominal pain]] - lower right
- [[Diarrhea]]
- [[rash]]